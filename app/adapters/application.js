import RESTAdapter from 'ember-data/adapters/rest';

export default RESTAdapter.extend({
  host: 'https://api.github.com',

  buildURL() {
    let url = this._super(...arguments);
    return url.replace("%2F", "/");
  }
});
